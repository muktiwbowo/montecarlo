package com.houseofdabu.montecarlo.holder

import android.support.v7.app.AlertDialog
import android.support.v7.widget.RecyclerView
import android.view.View
import com.houseofdabu.montecarlo.controller.database
import com.houseofdabu.montecarlo.model.ModelMaster
import kotlinx.android.synthetic.main.holder_master.view.*
import org.jetbrains.anko.alert
import org.jetbrains.anko.noButton
import org.jetbrains.anko.yesButton

class HolderMaster(view: View): RecyclerView.ViewHolder(view) {
    fun bindMaster(modelMaster: ModelMaster){
        itemView.holder_master_id.text          = modelMaster.masterId
        itemView.holder_master_month.text       = modelMaster.masterMonth
        itemView.holder_master_year.text        = modelMaster.masterYear
        itemView.holder_master_visitors.text    = modelMaster.masterVisitors

        itemView.setOnClickListener {
            itemView.context.alert ("Yakin hapus data ini ?"){
                title = "Delete Data"
                yesButton {

                }
                noButton {

                }
            }.show()
        }
    }
}