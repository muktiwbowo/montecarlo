package com.houseofdabu.montecarlo.adapter

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.ViewGroup
import com.houseofdabu.montecarlo.R
import com.houseofdabu.montecarlo.holder.HolderMaster
import com.houseofdabu.montecarlo.model.ModelMaster

class AdapterMaster(private val listData: List<ModelMaster>): RecyclerView.Adapter<HolderMaster>() {
    override fun onCreateViewHolder(parent: ViewGroup, position: Int): HolderMaster {
        val view = LayoutInflater.from(parent.context)
            .inflate(R.layout.holder_master, parent, false)
        return HolderMaster(view)
    }

    override fun getItemCount(): Int = listData.size

    override fun onBindViewHolder(holderMaster: HolderMaster, position: Int) {
        holderMaster.bindMaster(listData[position])
    }
}