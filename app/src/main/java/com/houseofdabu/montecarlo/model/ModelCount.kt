package com.houseofdabu.montecarlo.model

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

@Parcelize
data class ModelCount (
    @SerializedName( "id_count")
    var idCount: Int?,
    @SerializedName( "sigma")
    var sigma: String?,
    @SerializedName( "mu")
    var mu: String?,
    @SerializedName( "delta_t")
    var deltaT: String?,
    @SerializedName( "random_number")
    var randomNumber: String?,
    @SerializedName( "deviation")
    var deviation: String?,
    @SerializedName( "years")
    var years: String?,
    @SerializedName( "simulation")
    var simulation: String?
):Parcelable