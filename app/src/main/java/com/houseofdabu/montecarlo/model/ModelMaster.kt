package com.houseofdabu.montecarlo.model

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

@Parcelize
data class ModelMaster(
    @SerializedName("_id")
    var id: Int,
    @SerializedName("master_id")
    var masterId: String,
    @SerializedName("master_month")
    var masterMonth: String?,
    @SerializedName("master_year")
    var masterYear: String?,
    @SerializedName("master_visitors")
    var masterVisitors: String?
): Parcelable {
    companion object {
        const val TABLE_MASTER: String      = "master_data"
        const val ID: String                = "id_"
        const val MASTER_ID: String        = "id_master"
        const val MASTER_MONTH: String      = "master_month"
        const val MASTER_YEAR: String       = "master_year"
        const val MASTER_VISITORS: String   = "master_visitors"
    }
}