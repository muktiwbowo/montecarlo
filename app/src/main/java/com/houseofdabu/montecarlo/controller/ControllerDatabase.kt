package com.houseofdabu.montecarlo.controller

import android.content.Context
import android.database.sqlite.SQLiteDatabase
import com.houseofdabu.montecarlo.model.ModelMaster
import org.jetbrains.anko.db.*

class ControllerDatabase(context: Context):
        ManagedSQLiteOpenHelper(context, "monte_carlo.db", null, 1) {
    companion object {
        private var mControllerDatabase: ControllerDatabase? = null

        @Synchronized
        fun getInstance(context: Context): ControllerDatabase{
            if (mControllerDatabase == null){
                mControllerDatabase = ControllerDatabase(context.applicationContext)
            }
            return mControllerDatabase!!
        }
    }
    override fun onCreate(db: SQLiteDatabase?) {
        db?.createTable(
            ModelMaster.TABLE_MASTER, true,
                ModelMaster.ID to INTEGER + PRIMARY_KEY + AUTOINCREMENT,
                ModelMaster.MASTER_ID to TEXT + UNIQUE,
                ModelMaster.MASTER_MONTH to TEXT,
                ModelMaster.MASTER_YEAR to TEXT,
                ModelMaster.MASTER_VISITORS to TEXT)
    }

    override fun onUpgrade(db: SQLiteDatabase?, oldVersion: Int, newVersion: Int) {
        db?.dropTable(ModelMaster.TABLE_MASTER, true)
    }
}
val Context.database: ControllerDatabase
    get() = ControllerDatabase.getInstance(applicationContext)