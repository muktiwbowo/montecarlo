package com.houseofdabu.montecarlo.activity

import android.database.sqlite.SQLiteConstraintException
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.LinearLayoutManager
import android.util.Log
import com.houseofdabu.montecarlo.R
import com.houseofdabu.montecarlo.adapter.AdapterMaster
import com.houseofdabu.montecarlo.controller.database
import com.houseofdabu.montecarlo.model.ModelMaster
import kotlinx.android.synthetic.main.activity_master.*
import org.jetbrains.anko.ctx
import org.jetbrains.anko.db.classParser
import org.jetbrains.anko.db.insert
import org.jetbrains.anko.db.select
import org.jetbrains.anko.toast

class ActivityMaster : AppCompatActivity() {

    private var             masterData: List<ModelMaster> = mutableListOf()
    private lateinit var    mAdapterMaster: AdapterMaster

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_master)
        initView()
        showData()
    }

    private fun initView(){
        master_save.setOnClickListener {
            saveMasterData()
        }
        master_show.setOnClickListener {
            showMasterData()
        }
    }

    private fun showData(){
        try{
            database.use {
                master_recycler_view.layoutManager  = LinearLayoutManager(ctx)
                mAdapterMaster                      = AdapterMaster(masterData)
                master_recycler_view.adapter        = mAdapterMaster
                val listMasterData                  = select(ModelMaster.TABLE_MASTER)
                masterData                            = listMasterData.parseList(classParser<ModelMaster>())
                mAdapterMaster.notifyDataSetChanged()
                Log.e("MasterData", ""+masterData.toList())
            }
        }catch (e: SQLiteConstraintException){
            e.localizedMessage
        }
    }

    private fun saveMasterData(){
        val id          = master_id.text
        val month       = master_month.text
        val year        = master_year.text
        val visitors    = master_visitor.text
        try {
            database.use {
                insert(
                    ModelMaster.TABLE_MASTER,
                    ModelMaster.MASTER_ID to id.toString(),
                    ModelMaster.MASTER_MONTH to month.toString(),
                    ModelMaster.MASTER_YEAR to year.toString(),
                    ModelMaster.MASTER_VISITORS to visitors.toString()
                )
            }
            toast("Successfully Inserted")
        }catch (e: SQLiteConstraintException){
            e.localizedMessage
            toast("Failed to insert")
        }
    }

    private fun showMasterData(){
        if (masterData.isEmpty()){
            toast("Data is empty, please insert data")
        } else{
            showData()
        }
    }
}
