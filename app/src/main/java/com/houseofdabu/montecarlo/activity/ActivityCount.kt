package com.houseofdabu.montecarlo.activity

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import com.houseofdabu.montecarlo.R

class ActivityCount : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_count)
        initToolbar()
    }

    override fun onSupportNavigateUp(): Boolean {
        finish()
        return super.onSupportNavigateUp()
    }

    fun initToolbar(){
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        supportActionBar?.title = "Home"
    }
}
