package com.houseofdabu.montecarlo.activity

import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import com.houseofdabu.montecarlo.R
import kotlinx.android.synthetic.main.activity_login.*

class ActivityLogin : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login)
        login_button.setOnClickListener {
            initLogin()
        }
    }

    fun initLogin(){
        val username = login_username.text
        val password = login_password.text
        if (username.contains("admin") && password.contains("admin")) {
            startActivity(Intent(this, ActivityHome::class.java))
        } else {
            Log.e("LoginActivity", "wrong username or password"+username+password)
        }
    }
}
