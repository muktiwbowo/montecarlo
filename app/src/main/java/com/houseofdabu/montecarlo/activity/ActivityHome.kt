package com.houseofdabu.montecarlo.activity

import android.annotation.SuppressLint
import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import com.houseofdabu.montecarlo.R
import kotlinx.android.synthetic.main.activity_home.*
import java.text.SimpleDateFormat
import java.util.*

class ActivityHome : AppCompatActivity() {

    @SuppressLint("SimpleDateFormat")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_home)
        initToolbar()
        initView()
    }

    override fun onSupportNavigateUp(): Boolean {
        finish()
        return super.onSupportNavigateUp()
    }

    fun initToolbar(){
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        supportActionBar?.title = "Home"
    }

    fun initView(){
        val calendar    = Calendar.getInstance()
        val dateFormat  = SimpleDateFormat("dd-MM-yyyy HH:mm:ss")
        home_date.text = dateFormat.format(calendar.time)
        home_count.setOnClickListener { startActivity(Intent(this, ActivityCount::class.java)) }
        home_master_data.setOnClickListener { startActivity(Intent(this, ActivityMaster::class.java)) }
        home_graphic.setOnClickListener { startActivity(Intent(this, ActivityGraphic::class.java)) }
    }
}
